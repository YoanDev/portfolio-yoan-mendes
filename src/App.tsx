import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Home from "./pages/home";

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route index path="/" Component={Home} />
      </Routes>
    </Router>
  );
};

export default App;
