import React from "react";
import styled from "styled-components";

export interface DummyAtomProps {
  children: React.ReactNode;
  dataTestId?: string;
}

/**
 * Dummy atom
 * @param {React.ReactNode} children child component display in the card
 */
export function DummyAtom({
  dataTestId = "dummy-atom",
  children,
}: DummyAtomProps) {
  return <StyledDummyAtom data-testid={dataTestId}>{children}</StyledDummyAtom>;
}

export default DummyAtom;

export const StyledDummyAtom = styled.div<DummyAtomProps>`
  display: flex;
`;
