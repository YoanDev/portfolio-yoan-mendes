import { render } from "@testing-library/react";
import DummyAtom from "./dummy-atom";

describe("DummyAtom", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<DummyAtom>test</DummyAtom>);
    expect(baseElement).toBeTruthy();
  });
});
