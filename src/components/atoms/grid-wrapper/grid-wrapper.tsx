import React from "react";
import styled from "styled-components";

export interface GridWrapperProps {
  children: React.ReactNode;
  dataTestId?: string;
  templateColumns?: string;
}

/**
 * Grid
 * @param {React.ReactNode} children items of the grid
 * @param {string} templateColumns css property grid-template-columns

 */
export function GridWrapper({
  dataTestId = "grid-wrapper",
  children,
}: GridWrapperProps) {
  return (
    <StyledGridWrapper data-testid={dataTestId}>{children}</StyledGridWrapper>
  );
}

export default GridWrapper;

export const StyledGridWrapper = styled.div<GridWrapperProps>`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  grid-gap: 10px;
`;
