import styled from "styled-components";
type Sizes = "40px" | "60px" | "105px";

export interface AvatarProps {
  src?: string;
  alt?: string;
  size?: Sizes;
}

/**
 * Display Avatar container
 * @param {string} src of the avatar
 * @param {string} alt of the avatar
 * @param {size}  size of the avatar
 */
export function Avatar({ alt, size, src }: AvatarProps) {
  return <ImageWrapper size={size} alt={alt} src={src} />;
}

const ImageWrapper = styled.img<AvatarProps>`
  display: block;
  width: ${(props) => (props.size ? props.size : "60px")};
  height: ${(props) => (props.size ? props.size : "60px")};
  border-radius: 50%;
  object-fit: cover;
  margin-right: 5px;
  @media only screen and (max-width: 715px) {
    width: 40px !important;
    height: 40px !important;
  }
`;
