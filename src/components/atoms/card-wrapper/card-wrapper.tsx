import React from "react";
import styled from "styled-components";

export interface CardWrapperProps {
  children: React.ReactNode;
  padding?: string;
  background?: string;
}

/**
 * Display white card container
 * @param {React.ReactNode} children child component display in the card
 * @param {string} padding padding for the component 20px by default
 * @param {string} background background for the component white by default
 */
export function CardWrapper({ padding, children }: CardWrapperProps) {
  return (
    <StyledCardWrapper data-testid="card-wrapper" padding={padding}>
      {children}
    </StyledCardWrapper>
  );
}

export default CardWrapper;

export const StyledCardWrapper = styled.div<CardWrapperProps>`
  display: flex;
  flex-direction: column;
  background: ${(props) => props.background || "20px"};
  border-radius: 8px;
  width: 100%;
  height: 100%;
  box-shadow: 0 3px 3px 1px rgb(0 0 0 / 20%);
  padding: ${(props) => props.padding || "20px"};
  justify-content: space-between;
  position: relative;
  overflow: hidden;
`;
