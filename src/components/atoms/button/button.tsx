import styled from "styled-components";

export interface ButtonProps {
  children: React.ReactNode;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  width?: string;
  height?: string;
  fontSize?: string;
  background?: string;
  textTransform?: string;
  dataTestId?: string;
}

/**
 * Display a button
 * @param {string | string[]} children children display inside the button
 * @param {React.MouseEventHandler<HTMLButtonElement>} onClick fonction trigger when the button is clicked
 * @param {string} width width of the button
 * @param {string} height height of the button
 * @param {string} dataTestId testId passed to the button element
 * @param {string} fontSize fontSize passed to the button element
 * @param {string} textTransform textTransform passed to the button element
 * @returns
 */
export function Button({
  children,
  onClick,
  height,
  width,
  fontSize,
  dataTestId,
  textTransform,
}: ButtonProps) {
  return (
    <StyledButton
      data-testid={dataTestId}
      height={height}
      width={width}
      fontSize={fontSize}
      textTransform={textTransform}
      onClick={onClick}
    >
      {children}
    </StyledButton>
  );
}

export default Button;

const StyledButton = styled.button<ButtonProps>`
  height: ${(props) => (props.height ? props.height : "30px")};
  width: ${(props) => (props.width ? props.width : "max-content")};
  font-size: ${(props) => (props.fontSize ? props.fontSize : "12px")};
  background: ${(props) => (props.background ? props.background : "#000000")};
  color: #fff;
  position: relative;
  padding: 0px 20px;
  z-index: 0;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  text-transform: ${(props) =>
    props.textTransform ? props.textTransform : "uppercase"};
  flex-direction: row;
  border: 1px solid transparent;
  border-radius: 20px;
  cursor: pointer;
  font-weight: bold;
  white-space: nowrap;
  overflow: hidden;
  box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.15);
  min-width: 60px;
`;
